#!/usr/bin/env python
# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# Universal Select Tool
# Profiling Tool
# uprofile.py mephx.x@gmail.com

import os
import re
import sys
import stat
import json
import traceback

from umodule import *
from uio import filesystem
from uio import printsystem


verbose = False
printsystem.set_type('profile')
filesystem.set_type('profile')

class Profile(Module):

	def __init__(self, name):
		self.name = name
		self.actions = []
		self.parameters = []
		self.output = []
		self.modules = []
		
		self.actions.append(Action(name = 'activate', \
			description = 'Set this profile for this folder.', \
			type = 'profile'))
		self.actions.append(Action(name = 'default', \
			description = 'Set this profile the default profile.', \
			type = 'profile'))

	def write_profile(self):
		print self.profile
		return

	def read_profile(self):
		str = ''
				
		for line in filesystem.read_file('.uprofile/' + self.name + '.json'):
			str += line
		
		profile = json.loads(str)
		
		self.profile = profile
		
		self.author = profile['profile']['author']
		self.version = profile['profile']['version']
		self.description = profile['profile']['description']
		modules = profile['profile']['modules']
		
		for module in modules:
			actions = []
			for action in modules[module]['actions']:
				actions.append([action, modules[module]['actions'][action]])
				
			module = self.get_module(module)
							
			self.modules.append([module, actions])
			
	def get_module(self, name):
		import modules
		modname = name
		modpath = 'modules.'+ modname
		__import__(modpath)
		module = eval(modpath + '.module')
		return module


	
class UniversalProfileTool:
	
	def __init__(self):
		self.profiles = []
		return
	
	def get_profiles(self):
		""" Returns the list of available uprofiles """
		if filesystem.path_exists('.uprofile'):
			for profile in filesystem.list_dir('.uprofile/'):
				match = re.match('(.+).json$', profile)
				if match:
					_profile = Profile(match.group(1))
					_profile.read_profile()
					self.profiles.append(_profile)
							
	def parse_argv(self, args):
		global verbose, version
		profile = None
		profiles = None
		action = None
		help = False
		list = False
		
		printsystem.use_colors(True)
		
		for arg in args:
			if arg == '-v':
				verbose = True
				printsystem.verbose()
				args = args[1:]
			elif arg == '-nc':
				printsystem.use_colors(False)
				args = args[1:]
			elif arg == '-help':
				help = True
				args = args[1:]
			elif arg == '-list':
				list = True
				args = args[1:]
		
		
		if list or help:
			self.get_profiles()
			profiles = self.profiles
		elif len(args) < 1:
			try:
				profile = Profile('folder')
				profile.read_profile()
				action = profile.get_action('activate')
				action.build()
				action.do_action(['activate'], profile.modules)
			except IOError:
				self.get_profiles()
				profiles = self.profiles
				list = True	
		elif len(args) == 1:
			try:
				profile = Profile(args[0])
				profile.read_profile()
			except IOError:
				printsystem.print_exception(Exception(\
					'No such option/profile "' + args[0] + \
					'"\n    "uprofile -help" for help'))
				self.get_profiles()	
				profiles = self.profiles
				list = True
		elif len(args) >= 2:
			try:
				profile = Profile(args[0])
				profile.read_profile()
				action = profile.get_action(args[1])
				action.build()
				action.do_action(args[1:], profile.modules)
			except IOError:
				printsystem.print_exception(Exception(\
					'No such option/profile "' + args[0] + \
					'"\n    "uprofile -help" for help'))
				self.get_profiles()	
				profiles = self.profiles
				list = True
				
		if len(args) == 2:
			args = None
		else:
			args = args[2:]
		
		return [profile, profiles, args, action, help, list]

		
def main():
	uprofile = UniversalProfileTool()
	try:
		list = uprofile.parse_argv(sys.argv[1:])
		
		printsystem.print_ui(profile = list[0], \
			profiles = list[1], action = list[3], \
			args = list[2], help = list[4], list = list[5])
		
	except UserWarning, warning:
		printsystem.print_exception(warning, True)
	except Exception, exception:
		printsystem.print_exception(exception)
		if verbose:
			traceback.print_exc()
			printsystem.print_line('')
		exit(1)
	exit(0)

if __name__ == '__main__': main()
