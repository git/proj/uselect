#!/usr/bin/env python
# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# Universal Select Tool
# Base Module
# umodule.py mephx.x@gmail.com

import os
import re
import sys
import stat
import string
import traceback


from uio import filesystem
from uio import printsystem

from umodule import *

verbose = False

version = '0.2'
modules_dir = '/usr/share/uselect/modules/'


class UniversalSelectTool:
	
	def __init__(self):

		return

	def get_modules(self):
		self.modules = []
		for module in filesystem.list_dir(modules_dir):
			if re.match('__init__.py$', module):
				continue
			match = re.match('(.*).py$', module)
			if match:
				import modules
				modname = match.group(1)
				modpath = 'modules.'+ modname
				__import__(modpath)
				module = eval(modpath + '.module')
				self.modules.append(module)		
	
	def get_module(self, name):
		import modules
		modname = name
		modpath = 'modules.'+ modname
		__import__(modpath)
		module = eval(modpath + '.module')
		return module

	def parse_argv(self, args):
		global verbose, version
		module = None
		modules = None
		action = None
		printsystem.use_colors(True)
		for arg in args:
			if arg == '-v':
				verbose = True
				printsystem.verbose()
				args = args[1:]
			elif arg == '-global':
				filesystem.set_global = True
				args = args[1:]
			elif arg == '-nc':
				printsystem.use_colors(False)
				args = args[1:]
			elif arg == '-version':
				printsystem.print_version(version)
				args = args[1:]			
		if len(args) < 1:
			self.get_modules()
			modules = self.modules
		elif len(args) == 1:
			module = self.get_module(args[0])
		elif len(args) >= 2:
			module = self.get_module(args[0])
			action = module.get_action(args[1])
			action.build()
			action.do_action(args[2:])
		if len(args) == 2:
			args = None
		else:
			args = args[2:]
		
		return [module, modules, args, action]


def main():
	uselect = UniversalSelectTool()
	try:
		list = uselect.parse_argv(sys.argv[1:])
		
		printsystem.print_ui(module = list[0], \
			modules = list[1], args = list[2], action = list[3])
	except UserWarning, warning:
		printsystem.print_exception(warning, True)
	except Exception, exception:
		printsystem.print_exception(exception)
		if verbose:
			traceback.print_exc()
			printsystem.print_line('')
		exit(1)
	exit(0)

if __name__ == '__main__': main()
