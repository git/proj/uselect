#!/usr/bin/env python
# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# python.uselect mephx.x@gmail.com

from umodule import *

module = Module(name = "gcc", description = "Python GCC Version Switcher", version = "0.1", author ="mephx.x@gmail.com") # We define the module
bin = Action (name = 'bin', description = "Change GCC's Version", type = "sym") # Define a Symlinking Action

gcc = Link(alias = "gcc", target_dir = "usr/bin/", target = "gcc", prefix = "/usr/bin/", regexp = "gcc-([0-9]+\.[0-9]+$)") 
#gcc_config = Link(alias = "gcc-config", target = "/usr/bin/gcc-config", prefix = "/usr/bin/", regexp = "gcc-config([0-9]+\.[0-9]+$)")

#gcc.add_link(gcc_config) # For inheritance testing
bin.add_link(gcc) # Adding The Link to the action
module.add_action(bin) #Adding the action to the module


