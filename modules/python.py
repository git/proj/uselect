#!/usr/bin/env python
# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# python.uselect mephx.x@gmail.com

from umodule import *

module = Module(name = "python", description = "Python Version Switcher", version = "0.1", author ="mephx.x@gmail.com") # We define the module


bin = Action (name = 'bin', description = "Change Python's Version", type = "sym") # Define a Symlinking Action
python = Link(alias = "python", target_dir = "usr/bin/", target = "python", prefix = "/usr/bin/", regexp = "python([0-9]+\.[0-9]+$)") 
python_config = Link(alias = "python-config", target_dir = "usr/bin/", target = "python-config", prefix = "/usr/bin/", regexp = "python([0-9]+\.[0-9]+)-config$")
python.add_link(python_config) # For inheritance
bin.add_link(python) # Adding The Link to the action

module.add_action(bin) #Adding the action to the module

test = Action (name = 'test', description = 'Test Python Environment', type = 'runnable')

test.add_parameter('<times>')
test.add_parameter('[<what>]') # optional notation

test.add_usage("""#!/usr/bin/python
print "Python Test will echo <times> * <what>"
print "Usage can be multi-line Yay!"
print ""
for i in range(5):
	print "Usage can have dynamic options! " + str(i)
""")

test.add_code("""#!/usr/bin/python
import sys

def is_number(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
		
argv = sys.argv[1:]
times = sys.argv[1:][0]
if is_number(times):
	times = int(times)	
	if len(argv) >= 2:
		what = sys.argv[1:][1]
	else: 
		what = 'Hello World...'
	print str(what) * times
else:
	print 'Invalid Option(s)'
	exit(1)
exit(0)
""")


module.add_action(test)

env = Action (name = 'env', description = 'Some env test', type = 'env')
path_var = env.get_var(name = 'PATH')

path_var.add_value(value = '~/.uselect/bin/')

module.add_action(env)
