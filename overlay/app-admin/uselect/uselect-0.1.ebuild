# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Universal Select Tool"
HOMEPAGE="http://web.ist.utl.pt/~sergio.almeida/"
SRC_URI="http://web.ist.utl.pt/~sergio.almeida/${P}.tar.gz"
# http://git.overlays.gentoo.org/gitweb/?p=proj/uselect.git&a=snapshot;h=master;sf=tgz


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

src_install() {
    dodir "${ROOT}usr/share/uselect"
    insinto "${ROOT}usr/share/uselect"
    doins uselect/*.py
    exeinto "${ROOT}usr/share/uselect"
    doins uselect/*.py
    doexe uselect/uselect.py
    doexe uselect/uprofile.py
    dodir "${ROOT}usr/share/uselect/modules"
    insinto "${ROOT}usr/share/uselect/modules"
    doins uselect/modules/*.py
    dosym "{$ROOT}"usr/share/uselect/uselect.py usr/bin/uselect
    dosym "{$ROOT}"/usr/share/uselect/uprofile.py usr/bin/uprofile

    # dodoc FAQ NEWS README || die
}
