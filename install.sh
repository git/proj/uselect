#!/bin/bash
# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# install.sh mephx.x@gmail.com

if [ "$(id -u)" != "0" ]; then
   echo "uselect's $0 must be run as root!"
   exit 1
fi

rm -f /usr/bin/uselect
rm -f /usr/bin/uprofile
rm -rf /usr/share/uselect
mkdir /usr/share/uselect
cp -R * /usr/share/uselect/
ln -s /usr/share/uselect/uselect.py /usr/bin/uselect
ln -s /usr/share/uselect/uprofile.py /usr/bin/uprofile
