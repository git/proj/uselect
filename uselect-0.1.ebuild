# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Universal Select Tool"
HOMEPAGE="http://web.ist.utl.pt/~sergio.almeida/"
SRC_URI="http://web.ist.utl.pt/~sergio.almeida/${P}.tar.gz"
# http://git.overlays.gentoo.org/gitweb/?p=proj/uselect.git&a=snapshot;h=master;sf=tgz


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

src_install() {
    dodir /usr/share/uselect
    cp -R "${S}/" "${D}/" || die "Install failed!"
    dolink "${ROOT}"/usr/bin/uselect "${ROOT}"/usr/share/uselect
	dolink "${ROOT}"/usr/bin/uprofile "${ROOT}"/usr/share/uprofile
	# dodoc FAQ NEWS README || die
}
